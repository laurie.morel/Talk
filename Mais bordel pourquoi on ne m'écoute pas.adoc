= Speaker : Timothée Chevrier @msieur_tim – Directeur de projet

= Inspiration : 

* Développement personnel 

Le speaker n’a pas l’impression de souffrir de problèmes de communication car il agit avec les autres selon des concepts clés:

* Agilité : Manifesto for Agile Software Development 
          
* Privilégier les individus et les interactions
          
* Sois le changement que tu veux voir dans le monde : Gandhi
    
== 1er concept : Cadres de référence

Le speaker nous a fait utiliser trois posts-it sur les thèmes suivants : voyage, travail et bonheur.

Chacun a sa propre représentation. C’est aussi valable pour des métiers, selon ses propres cadres de références.

Un développeur voit un chef de projet, un graphiste ou autre selon sa propre référence, par exemple.

Nous sommes faits, pour ne pas voir la même chose. Nous avons forcément une vision différente des choses. 

Il est important de l’avoir en tête et de réussir à accéder à celui de l’autre, en faisant parfois le deuil, de son propre cadre de référence. 

Il est important d’ouvrir son cadre de référence, à une autre perception. 

== 2ème concept : Positions de vie

Le speaker parle alors de positions de vie, entre deux individus différents. Chacun est dans une certaine posture, lorsqu'il s'adresse à une autre personne.

Lors d'un échange entre deux personnes, on peut retrouver les postures suivantes:

* Nok - Nok : Contre, passivité, désespoir, impasse, fatalisme, abandon

* Non - OK : Pour l'autre, dépendance, infériorité, honte, admiration, soumission

* OK - Non : Domination, indépendant, supériorité, mépris, pitié, compétition

* OK - OK : Avec l'autre, autonomie, alter-dépendance, collaboration, solidarité, gagnant-gagnant

La décision de se mettre en « ok ok » avec quelqu’un, est responsable de la création de belles rencontres.

Il s’agit d’un choix personnel, à un moment t. 

Cette décision permet de casser des barrières et des aprioris et se rediriger vers une posture naturelle. 

== 3ème concept : Great Gift

L’espace dans lequel tout est facile et tout est naturel. 

Comprendre, ce pourquoi, on est fait. Comprendre, notre talent.

Il s’agit d’un enjeu, qui peut avoir un impact assez fort dans une vie.

Il s’agit de mettre son talent au cœur de la mission que l’on nous confie. 

Une fois que cette démarche est faite d’un point de vue personnel, il est plus facile de visualiser le talent, de l’autre. 

Animer des groupes pour apprendre ensemble

Livre à conseiller : L’élément, quand trouver sa voie, peut tout changer ! L’auteur est Ken Robinson.

== 4ème concept : Intelligence collective

L’intelligence collective est possible que si chaque individu veut y participer et y trouver sa place. 

Un manager, ne peut rien imposer.

Les 3 concepts décrits précédemment permettent d’accéder à l’intelligence collective.


Construire sa boîte à outils : points forts, analyse transactionnelle, meta communication, CNV, écoute active, langage non verbal, facilitation graphique, empathie, story stelling